//
//  Extension.swift
//  TheNewFlickr
//
//  Created by ahmed gado on 18/07/2021.
//

import UIKit
import Kingfisher

extension UITableView {
    
    func registerCellNib<Cell: UITableViewCell>(cellClass: Cell.Type){
        self.register(UINib(nibName: String(describing: Cell.self), bundle: nil), forCellReuseIdentifier: String(describing: Cell.self))
    }
    
    
    func dequeue<Cell: UITableViewCell>() -> Cell{
        let identifier = String(describing: Cell.self)
        
        guard let cell = self.dequeueReusableCell(withIdentifier: identifier) as? Cell else {
            fatalError("Error in cell")
        }
        
        return cell
    }
    
}

extension UICollectionView {
    
    func registerCell<Cell: UICollectionViewCell>(cellClass: Cell.Type){
        //MARK: Generic Register cells
        self.register(UINib(nibName: String(describing: Cell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: Cell.self))
    }
    
    
    func dequeue<Cell: UICollectionViewCell>(indexPath: IndexPath) -> Cell{
        let identifier = String(describing: Cell.self)
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? Cell else {
            fatalError("Error in cell")
        }
        return cell
    }
    

}
extension UIApplication {
    
    static func topViewController(base: UIViewController? = UIApplication.shared.delegate?.window??.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return topViewController(base: selected)
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }
}
class Helper{
    
    static var helper = Helper()
    
    func setImageWithg(image: String,withImageView theIV: UIImageView, andPlaceholder theplcaeholder: UIImage?)  {
        // set the image
        let imageURL = image
        let optionsInfos: KingfisherOptionsInfo = [.transition(ImageTransition.fade(1))]
        if let url = URL(string: imageURL) {
            //                        theIV.kf.indicatorType = .activity
            theIV.kf.setImage(with: url, placeholder: theplcaeholder, options: optionsInfos, progressBlock: nil)
        }
    }
}
extension UIViewController {
    func goPush<T:UIViewController>( vc : T.Type )  {
        let identifer = String(describing: T.self)
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: identifer) as? T{
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
