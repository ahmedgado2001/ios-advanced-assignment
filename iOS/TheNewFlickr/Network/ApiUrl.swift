//
//  ApiUrl.swift
//  TheNewFlickr
//
//  Created by ahmed gado on 18/07/2021.
//

import UIKit
struct ApiUrl {
    static var image_id = "1"
    static let baseURL =  "https://www.flickr.com/"
    static let photoUrl = "services/rest/?method=flickr.photos.search&api_key=94b0a1ad9d4f1aebf9f2f2c006fb4c65&text=cat&format=json&nojsoncallback=1"
    static let photoDetails = "services/rest/?method=flickr.photos.getSizes&api_key=94b0a1ad9d4f1aebf9f2f2c006fb4c65&photo_id=\(image_id)&format=json&nojsoncallback=1"
}
