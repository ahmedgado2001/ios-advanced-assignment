//
//  UploadData.swift
//  App
//
//  Created by ahmed gado on 06/01/2021.
//  Copyright © 2021 ahmed gado. All rights reserved.
//

import Foundation
struct UploadData {
    var data: Data
    var fileName, mimeType, name: String
}
