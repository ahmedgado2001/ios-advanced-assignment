//
//  ResponseHandler.swift
//  App
//
//  Created by ahmed gado on 06/01/2021.
//  Copyright © 2021 ahmed gado. All rights reserved.
//

import Foundation
import Alamofire

protocol HandleAlamoResponse {
    /// Handles request response, never called anywhere but APIRequestHandler
    ///
    /// - Parameters:
    ///   - response: response from network request, for now alamofire Data response
    ///   - completion: completing processing the json response, and delivering it in the completion handler
    func handleResponse<T: CodableInit>(_ response: AFDataResponse<Data>, completion: CallResponse<T>)
}

extension HandleAlamoResponse {
    
    func handleResponse<T: CodableInit>(_ response: AFDataResponse<Data>, completion: CallResponse<T>) {
        switch response.result {
        case .failure(let error):
            guard let data = response.data else { return }
            guard let statusCode = response.response?.statusCode else { return }
            switch statusCode {
            case 400..<500:
                do {
//                    let messageError = try DefaultResponse(data: data)
                    completion?(.failure(APIError.errorMessage(message: error.localizedDescription )))
                } catch {
                    completion?(.failure(error))
                }
            default:
                completion?(.failure(error))
                let alert = UIAlertController(title: "The Internet is bad or weak", message: "Check your network connection", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
            }
        case .success(let value):
            do {
//                let defaultResponse = try DefaultResponse(data: value)
                let modules = try T(data: value)
                completion?(.success(modules))
//                if defaultResponse.status == "1" {
//                 
//                } else if defaultResponse.status == "2" {
//                        let modules = try T(data: value)
//                        completion?(.success(modules))
//                }else {
//                    completion?(.failure(APIError.errorMessage(message: defaultResponse.msg ?? "")))
//                }
            } catch let jsonError {
                print("Status Code is: \(response.response?.statusCode ?? 000000) \(jsonError)")
                completion?(.failure(jsonError))
            }
        }
    }
}

