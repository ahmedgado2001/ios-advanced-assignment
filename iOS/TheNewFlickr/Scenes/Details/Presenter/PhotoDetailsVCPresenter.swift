//
//  PhotoDetailsVCPresenter.swift
//  TheNewFlickr
//
//  Created by ahmed gado on 19/07/2021.
//

import Foundation
protocol PhotoDetailsView  : AnyObject {
    func showLoading()
    func hideLoading()
    func reloadData()
    func showError(error : String)
    func getDetailsData(data: Size)

}
protocol PhotoDetailsCellView   {
    func displayName(name : String)
}

class PhotoDetailsVCPresenter {

    private  weak var view : PhotoDetailsView?
    private var detailsSource = [Size]()
    init(view : PhotoDetailsView) {
        self.view = view
        getPhotoDetails()
    }
    
    
    func getPhotoDetails(){
        view?.showLoading()
        PhotoRouter.getphotoDetails.send(PhotoDetailsModel.self){ [weak self](response) in
            guard let self = self else{return}
            self.view?.hideLoading()
            switch response{
            case .success(let value):
                print(value.sizes)
                self.detailsSource = value.sizes.size
                self.view?.reloadData()
            case .failure(let error):
                guard let errorMessage = error as? APIError else {
                    // server Error
                    print(error.localizedDescription)
                    return
                }
                // BackEnd Error
                self.view?.showError(error: errorMessage.localizedDescription)
                
            }
        }
    }
    
    func getPhotoDetailsCount() -> Int {
        return detailsSource.count
    }
   

    
    func configerCell (cell : PhotoDetailsCellView , index : Int){
        let details = detailsSource[index]
        cell.displayName(name: details.label)
    }
    func didSelectRow(index: Int) {
        let data = detailsSource[index]
        view?.getDetailsData(data: data)
           view?.reloadData()
       }
    
}
