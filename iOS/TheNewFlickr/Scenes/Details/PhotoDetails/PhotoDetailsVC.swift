//
//  PhotoDetailsVC.swift
//  TheNewFlickr
//
//  Created by ahmed gado on 19/07/2021.
//

import UIKit
import SKActivityIndicatorView
class PhotoDetailsVC: UIViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    var presenter : PhotoDetailsVCPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = PhotoDetailsVCPresenter(view: self)
        print( ApiUrl.image_id)
        setupCollectionView()
    }
    func setupCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.registerCell(cellClass: SizeCollectionViewCell.self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PhotoDetailsVC : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.getPhotoDetailsCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(indexPath: indexPath) as SizeCollectionViewCell
        presenter.configerCell(cell: cell, index: indexPath.row)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {

        let numOfColumns : CGFloat = 4
        let spaceBetweenCells : CGFloat = 0
            let padding : CGFloat = 0
            let cellDimension = ((collectionView.bounds.width - padding) - (numOfColumns - 1) * spaceBetweenCells) / numOfColumns
            return CGSize(width: Int(cellDimension) - 8, height: 50)

    }
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.didSelectRow(index: indexPath.row)

    }
    
}
extension PhotoDetailsVC : PhotoDetailsView {
    func getDetailsData(data: Size) {
        Helper.helper.setImageWithg(image: data.source, withImageView: avatarImageView, andPlaceholder: nil)
        titleLabel.text = data.label
        sizeLabel.text = "\( data.height ) * \(data.width)"
        dateLabel.text = "\(data.media)"
    }
    
 
    func showLoading() {
        SKActivityIndicator.show()
    }
    
    func hideLoading() {
        SKActivityIndicator.dismiss()
    }
    
    func reloadData() {
        collectionView.reloadData()
    }
    
    func showError(error: String) {
        print(error)
    }
}
