//
//  SizeCollectionViewCell.swift
//  TheNewFlickr
//
//  Created by ahmed gado on 19/07/2021.
//

import UIKit

class SizeCollectionViewCell: UICollectionViewCell , PhotoDetailsCellView {
   
    

    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLabel.layer.borderColor = UIColor.black.cgColor
        titleLabel.layer.borderWidth = 1
        titleLabel.layer.cornerRadius = 8
        titleLabel.layer.masksToBounds = true
    }
    func displayName(name: String) {
        titleLabel.text = name
    }
}
