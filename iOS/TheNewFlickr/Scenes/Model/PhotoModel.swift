//
//  PhotoModel.swift
//  TheNewFlickr
//
//  Created by ahmed gado on 18/07/2021.
//

import Foundation
// MARK: - PhotoModel
struct PhotoModel: Codable , CodableInit {
    let photos: Photos
    let stat: String
}

// MARK: - Photos
struct Photos: Codable {
    let page, pages, perpage, total: Int
    let photo: [Photo]
}

// MARK: - Photo
struct Photo: Codable {
    let id, owner, secret, server: String
    let farm: Int
    let title: String
    let ispublic, isfriend, isfamily: Int
}
// MARK: - PhotoDetailsModel
struct PhotoDetailsModel: Codable , CodableInit {
    let sizes: Sizes
    let stat: String
}

// MARK: - Sizes
struct Sizes: Codable {
    let canblog, canprint, candownload: Int
    let size: [Size]
}

// MARK: - Size
struct Size: Codable {
    let label: String
    let width, height: Int
    let source: String
    let url: String
    let media: Media
}

enum Media: String, Codable {
    case photo = "photo"
}
