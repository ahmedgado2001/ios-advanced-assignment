//
//  ViewController.swift
//  TheNewFlickr
//
//  Created by Abdoelrhman Eaita on 13/07/2021.
//

import UIKit
import SKActivityIndicatorView


class PhotoVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var presenter : PhotoVCPresenter!
    let searchBar = UISearchBar()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        presenter = PhotoVCPresenter(view: self)
        setupTableView()
        setupSearchBar()
        title = "TheNewFlickr"
      
    }
    func setupSearchBar(){
        searchBar.frame = CGRect(x: 0, y: 0, width: 200, height: 70)
        searchBar.showsCancelButton = false
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBar.Style.default
        searchBar.placeholder = " Search Here....."
        searchBar.sizeToFit()
        tableView.tableHeaderView = searchBar
    }
    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.prefetchDataSource = self
        tableView.registerCellNib(cellClass: PhotoTableViewCell.self)
    }

}

extension PhotoVC: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        print(indexPaths)
        for index in indexPaths {
            if index.row >= presenter.getPhotoCount() - 2 {
                presenter.paginationPhoto { isSuccess in
                    if isSuccess {
                        presenter.getPhoto()
                    }
                }
                break
            }
        }
    }
    
    
}
extension PhotoVC: UITableViewDataSource, UITableViewDelegate {
  
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getPhotoCount()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue() as PhotoTableViewCell
        presenter.configerCell(cell: cell, index: indexPath.row)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(index: indexPath.row)
    }
    
}
extension PhotoVC: PhotoView {
    func navigateDetailsScreen(photo: Photo) {
        ApiUrl.image_id = photo.id
        goPush(vc: PhotoDetailsVC.self)
    }
    
    func showLoading() {
        SKActivityIndicator.show()
    }
    
    func hideLoading() {
        SKActivityIndicator.dismiss()
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    
    func showError(error: String) {
        print(error)
    }
    
    
}

extension PhotoVC : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        presenter.removePhotoSource()
        if searchText == "" {
            presenter.getPhoto()
        }
        for i in presenter.getPhotoSource() {
            if i.title.uppercased().contains(searchText.uppercased()){
                 presenter.updatePhotoSource(title: i.title)
            }
        }
    }
  
    
}
