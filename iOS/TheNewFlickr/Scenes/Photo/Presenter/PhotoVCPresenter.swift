//
//  PhotoVCPresenter.swift
//  TheNewFlickr
//
//  Created by ahmed gado on 18/07/2021.
//

import Foundation

protocol PhotoView  : AnyObject {
    func showLoading()
    func hideLoading()
    func reloadData()
    func showError(error : String)
    func navigateDetailsScreen(photo: Photo)

}
protocol PhotoCellView   {
    func displayName(name : String)
    func displayPhoto(photo : String)
}
class PhotoVCPresenter {
    private  weak var view : PhotoView?
    private var photoSource = [Photo]()
    private var currentPage = 1
    private var isLoadingData = false
    init(view : PhotoView) {
        self.view = view
        getPhoto()
    }
    
    func paginationPhoto(completion:(Bool) -> ()) {
        if !isLoadingData {
            completion(true)
        }else{
            completion(false)
        }
    }
    
    func getPhoto(){
        view?.showLoading()
        isLoadingData = true
        PhotoRouter.getPhoto(page: "\(currentPage)").send(PhotoModel.self){ [weak self](response) in
            guard let self = self else{return}
            self.view?.hideLoading()
            switch response{
            case .success(let value):
                print(value.photos)
                self.isLoadingData = false
                self.photoSource = value.photos.photo
                self.currentPage += 1
                self.view?.reloadData()
            case .failure(let error):
                
                guard let errorMessage = error as? APIError else {
                    // server Error
                    print(error.localizedDescription)
                    return
                }
                // BackEnd Error
                self.view?.showError(error: errorMessage.localizedDescription)
                
            }
        }
    }
    
    
    
    func getPhotoCount() -> Int {
        return photoSource.count
    }
    func getPhotoSource () -> [Photo]{
        return photoSource
    }
    func updatePhotoSource(title : String)    {
        let source = [Photo.init(id: "", owner: "", secret: "", server: "", farm: 0, title: title, ispublic: 0, isfriend: 0, isfamily: 0)]
        photoSource = source
        view?.reloadData()
    }
    func removePhotoSource()  {
        photoSource.removeAll()
        view?.reloadData()
    }
    
    
    
    
    func configerCell (cell : PhotoCellView , index : Int){
        let photo = photoSource[index]
        cell.displayName(name: photo.title)
        let url = "https://live.staticflickr.com/65535/\(photo.id)_26142fc994_s.jpg"
        cell.displayPhoto(photo: url)
    }
    func didSelectRow(index: Int) {
        let photo = photoSource[index]
           view?.navigateDetailsScreen(photo: photo)
       }
}
