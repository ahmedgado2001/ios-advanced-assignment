//
//  PhotoTableViewCell.swift
//  TheNewFlickr
//
//  Created by ahmed gado on 18/07/2021.
//

import UIKit

class PhotoTableViewCell: UITableViewCell , PhotoCellView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }
    func displayName(name: String) {
        titleLabel.text = name
    }
    
    func displayPhoto(photo: String) {
        Helper.helper.setImageWithg(image: photo, withImageView: photoImageView, andPlaceholder: #imageLiteral(resourceName: "jbjbjbjj"))
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
