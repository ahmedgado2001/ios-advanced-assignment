//
//  PhotoRouter.swift
//  TheNewFlickr
//
//  Created by ahmed gado on 18/07/2021.
//

import Foundation

import Foundation
import Alamofire
enum PhotoRouter: URLRequestBuilder {
    case getPhoto(page : String)
    case getphotoDetails
    // MARK: - Path
    internal var path: String {
        switch self {
        case .getPhoto:
            return ApiUrl.photoUrl
        case .getphotoDetails:
            return ApiUrl.photoDetails
        }
    }
    
    // MARK: - Parameters
    internal var parameters: Parameters? {
        var params = Parameters.init()
        switch self {
        case .getPhoto(let page):
            params["page"] = page
        case .getphotoDetails: break
            
        }
        return params
    }
    // MARK: - Header
    internal  var headers: HTTPHeaders {
        let header = HTTPHeaders()
        return header
    }
    // MARK: - Methods
    internal var method: HTTPMethod {
        return .get
    }
    
}

